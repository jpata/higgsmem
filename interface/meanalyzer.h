// In this file, we declare our own custom analyzers, based on the interface
// defined in nanoflow.h
#ifndef MATRIXELEMENTANALYZER_H
#define MATRIXELEMENTANALYZER_H

#include <TLorentzVector.h>

#include "nanoflow.h"
#include "myanalyzers.h"
#include "me_hmumu.h"

class MEMEventAnalyzer : public Analyzer {
 public:
  Output& output;
  double sqrt_s;

  MatrixElementHiggsMuMu memcalc;

  MEMEventAnalyzer(Output& _output, double _sqrt_s, string mg_card_path);
  virtual void analyze(NanoEvent& _event) override;
  virtual const string getName() const override;
  vector<GenParticle> get_particles_idx(MEMEvent& event,
                                        vector<unsigned int>& final_mu_idx);
  void match_muons(MEMEvent& event, vector<GenParticle>& gen, vector<Muon>& reco);
  void count_matched_mu(MEMEvent& event, vector<Muon>& reco);
};


#endif
