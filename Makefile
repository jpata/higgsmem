OPTS=-Wno-unsequenced -fPIC -Wall -O3
LIBS=-lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -lROOTDataFrame -lROOTVecOps -rdynamic

#linking to madgraph
CFLAGS_MG=-I./src/madgraph

#final compiler and linker flags
ROOT_CFLAGS=`root-config --cflags`
ROOT_LIBDIR=`root-config --libdir`
CFLAGS=${ROOT_CFLAGS} ${OPTS} -I./interface/ ${CFLAGS_MG}
LDFLAGS=-L${ROOT_LIBDIR} ${LIBS} ${OPTS}

#list of all objects for libraries
LIBAMP_HMM_DEPS = bin/me_hmumu.o bin/rambo.o bin/read_slha.o bin/ProcessGGH.o bin/ProcessQQZ.o bin/Parameters_sm__hgg_plugin_full.o bin/HelAmps_sm__hgg_plugin_full.o
LOOPER_DEPS = bin/looper.o bin/meanalyzer.o bin/myanalyzers.o

all: bin/libamp_hmm.so bin/libhiggsmem.so bin/looper bin/df

#objects
bin/%.o: src/madgraph/%.cc
	$(CXX) -c $(CFLAGS) $< -o $@

bin/%.o: src/%.cc
	$(CXX) -c $(CFLAGS) $< -o $@

#libraries
bin/libamp_hmm.so: $(LIBAMP_HMM_DEPS)
	$(CXX) ${CFLAGS} ${LDFLAGS} $(LIBAMP_HMM_DEPS) -shared -o $@

bin/libhiggsmem.so: bin/meanalyzer.o bin/myanalyzers.o
	$(CXX) ${LDFLAGS} ${LDFLAGS} bin/meanalyzer.o bin/myanalyzers.o -shared -o $@

#executables
bin/looper: $(LOOPER_DEPS) $(LIBAMP_HMM_DEPS)
	$(CXX) ${LDFLAGS} ${LIBAMP_HMM_DEPS} $(LOOPER_DEPS) -o bin/looper

bin/df: src/dataframe.cc
	$(CXX) ${CFLAGS} ${LDFLAGS} src/dataframe.cc -o bin/df

#misc
format: ${SRC_FILES} ${HEADER_FILES}
	clang-format -i -style=Google ${SRC_FILES} ${HEADER_FILES}

clean:
	rm -Rf bin/*

.PHONY: clean
