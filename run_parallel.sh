#!/bin/bash

DATADIR=/fast/tmp1

#Runs the looper step, computing the MEM on all events
find $DATADIR/jobs/dyjets_ll/ -name "*.json" | parallel --gnu -j16 ./bin/looper {} {}.out
#Merges the output into a big file
find $DATADIR/jobs/dyjets_ll/ -name "*.root" | xargs hadd -f $DATADIR/dyjets_ll.root
./bin/df /fast/tmp1/dyjets_ll.root out_dyjets.root

find $DATADIR/jobs/ggh_hmumu/ -name "*.json" | parallel --gnu -j16 ./bin/looper {} {}.out
find $DATADIR/jobs/ggh_hmumu/ -name "*.root" | xargs hadd -f $DATADIR/ggh_hmumu.root
./bin/df /fast/tmp1/ggh_hmumu.root out_ggh.root
