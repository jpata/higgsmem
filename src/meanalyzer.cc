#include "meanalyzer.h"

MEMEventAnalyzer::MEMEventAnalyzer(Output& _output,
                                                       double _sqrt_s, string mg_card_path)
    : output(_output), sqrt_s(_sqrt_s), memcalc(mg_card_path) {
  cout << "Creating MEMEventAnalyzer" << endl;

}

bool has_mother(const ROOT::VecOps::RVec<int>& genPartMothers, const ROOT::VecOps::RVec<int>& pdgIds, int start_idx, function<bool(int)> test_func) {
    const auto mother_idx = genPartMothers.at(start_idx);
    if (mother_idx == -1) {
      return false;
    } else if (mother_idx >= 0 && mother_idx < genPartMothers.size()){
      const auto mother_pdgId = pdgIds.at(mother_idx);
      if (test_func(mother_pdgId)) {
        return true;
      }
    } else {
      assert(false && "mother_idx is incorrect");
    }
    return has_mother(genPartMothers, pdgIds, mother_idx, test_func);
}

void MEMEventAnalyzer::analyze(NanoEvent& _event) {
  auto& event = static_cast<MEMEvent&>(_event);

  event.lc_uint.read(string_hash("nGenPart"));
  event.lc_vint.read(string_hash("GenPart_pdgId"));
  event.lc_vint.read(string_hash("GenPart_status"));
  event.lc_vint.read(string_hash("GenPart_genPartIdxMother"));
  event.lc_vfloat.read(string_hash("GenPart_pt"));
  event.lc_vfloat.read(string_hash("GenPart_eta"));
  event.lc_vfloat.read(string_hash("GenPart_phi"));
  event.lc_vfloat.read(string_hash("GenPart_mass"));
  event.lc_float.read(string_hash("Generator_x1"));
  event.lc_float.read(string_hash("Generator_x2"));

  // reconstruct initial state
  const auto x1 = event.lc_float.get(string_hash("Generator_x1"));
  const auto x2 = event.lc_float.get(string_hash("Generator_x2"));

  const auto E1 = sqrt_s * x1 / 2.0;
  const auto E2 = sqrt_s * x2 / 2.0;
  const auto pz1 = E1;
  const auto pz2 = -E2;
  vector<int> initial_pdgId;

  vector<unsigned int> mediator_idx;
  vector<unsigned int> final_mu_idx;

  const auto GenPart_pdgIds = event.lc_vint.get_vec(string_hash("GenPart_pdgId"));
  const auto GenPart_genPartIdxMother = event.lc_vint.get_vec(string_hash("GenPart_genPartIdxMother"));

  const auto nGenPart = event.lc_uint.get(string_hash("nGenPart"));

  for (unsigned int _nGenPart = 0; _nGenPart < nGenPart; _nGenPart++) {
    const auto status =
        event.lc_vint.get(string_hash("GenPart_status"), _nGenPart);
    const auto pdgId = GenPart_pdgIds[_nGenPart];
    // const auto pt = event.lc_vfloat.get(string_hash("GenPart_pt"),
    // _nGenPart);

    // Initial state
    if (status == 21) {
      initial_pdgId.push_back(pdgId);

      // intermediate or mediator particle
    } else if ((status == 22 && (pdgId == 25 || pdgId == 23))) {
      mediator_idx.push_back(_nGenPart);
      // final state particle
    } else if (status == 1 && abs(pdgId)==13) {

      const auto has_mother_H_or_Z = has_mother(GenPart_genPartIdxMother, GenPart_pdgIds, _nGenPart, [](int mother_id){ return (mother_id == 23) || (mother_id == 25);});
      if (has_mother_H_or_Z) {
        final_mu_idx.push_back(_nGenPart);
      }
    }
  }
    
  if(initial_pdgId.size() != 2) {
    cerr << "Expected 2 initial state particles but found " << initial_pdgId.size() << endl;
    throw std::runtime_error("MEMEventAnalyzer::analyze");
  }

  //assert(initial_pdgId.size() == 2);
  
  event.geninitialstate.push_back(GenParticleInitial(pz1, initial_pdgId.at(0)));
  event.geninitialstate.push_back(GenParticleInitial(pz2, initial_pdgId.at(1)));

  TLorentzVector i1_gen(0, 0, pz1, E1);
  TLorentzVector i2_gen(0, 0, pz2, E2);

  event.mediators = get_particles_idx(event, mediator_idx);

  event.genfinalstatemuon = get_particles_idx(event, final_mu_idx);
  match_muons(event, event.genfinalstatemuon, event.muons);

  if (event.geninitialstate.size() == 2 && event.genfinalstatemuon.size() == 2) {
    
    TLorentzVector mu1 = make_lv(event.genfinalstatemuon.at(0).pt(), event.genfinalstatemuon.at(0).eta(), event.genfinalstatemuon.at(0).phi(), event.genfinalstatemuon.at(0).mass());
    TLorentzVector mu2 = make_lv(event.genfinalstatemuon.at(1).pt(), event.genfinalstatemuon.at(1).eta(), event.genfinalstatemuon.at(1).phi(), event.genfinalstatemuon.at(1).mass());

    auto total_fs = mu1+mu2;
    auto boost_beta = -TVector3(total_fs.Px()/total_fs.E(), total_fs.Py()/total_fs.E(), 0.0);
    mu1.Boost(boost_beta);
    mu2.Boost(boost_beta);

    auto total = i1_gen + i2_gen - (mu1 + mu2);

    auto mevalues = memcalc.compute_me_initial_final_mumu(i1_gen, i2_gen, mu1, mu2);

    event.me_gen_sig = mevalues.ggh_hmumu;
    event.me_gen_bkg = mevalues.qqz_zmumu;
  }

   if (event.muons.size() >= 2) {
     auto mu1 = make_lv(event.muons.at(0).pt(), event.muons.at(0).eta(), event.muons.at(0).phi(), event.muons.at(0).mass());
     auto mu2 = make_lv(event.muons.at(1).pt(), event.muons.at(1).eta(), event.muons.at(1).phi(), event.muons.at(1).mass());
     
     auto mevalues = memcalc.compute_me_final_mumu(mu1, mu2);

     event.me_reco_sig = mevalues.ggh_hmumu;
     event.me_reco_bkg = mevalues.qqz_zmumu;
     event.reco_fs_pz = mevalues.reco_fs_pz;
   }
}

vector<GenParticle> MEMEventAnalyzer::get_particles_idx(
    MEMEvent& event, vector<unsigned int>& gen_idx) {
  vector<GenParticle> ret;
  for (auto idx : gen_idx) {
    GenParticle gp(event.lc_vfloat.get(string_hash("GenPart_pt"), idx),
                   event.lc_vfloat.get(string_hash("GenPart_eta"), idx),
                   event.lc_vfloat.get(string_hash("GenPart_phi"), idx),
                   event.lc_vfloat.get(string_hash("GenPart_mass"), idx),
                   event.lc_vint.get(string_hash("GenPart_pdgId"), idx));
    ret.push_back(gp);
  }
  return ret;
}

void MEMEventAnalyzer::match_muons(MEMEvent& event,
                                             vector<GenParticle>& gen,
                                             vector<Muon>& reco) {
  vector<TLorentzVector> lvec_recolep;
  vector<TLorentzVector> lvec_genlep;

  for (unsigned int i = 0; i < reco.size(); i++) {
    lvec_recolep.push_back(make_lv(reco.at(i).pt(), reco.at(i).eta(),
                                   reco.at(i).phi(), reco.at(i).mass()));
  }

  for (unsigned int i = 0; i < gen.size(); i++) {
    lvec_genlep.push_back(make_lv(gen.at(i).pt(), gen.at(i).eta(),
                                  gen.at(i).phi(), gen.at(i).mass()));
  }

  for (unsigned int i = 0; i < lvec_genlep.size(); i++) {
    double min_dr = 99.0;
    unsigned int best_match_reco = 0;
    bool found_match = false;

    for (unsigned int j = 0; j < lvec_recolep.size(); j++) {
      const auto dr = lvec_genlep.at(i).DeltaR(lvec_recolep.at(j));
      if ((dr < 0.3) && (dr < min_dr)) {
        best_match_reco = j;
        min_dr = dr;
        found_match = true;
      }
    }
    if (found_match) {
      reco.at(best_match_reco).matchidx = static_cast<int>(i);
    }
  }

  count_matched_mu(event, reco);
}

void MEMEventAnalyzer::count_matched_mu(MEMEvent& event,
                                                  vector<Muon>& reco) {
  for (auto& p : reco) {
    event.nMuon += 1;
    if (p.matchidx != -1) {
      event.nMuon_match += 1;
    }
  }
}

const string MEMEventAnalyzer::getName() const {
  return "MEMEventAnalyzer";
}