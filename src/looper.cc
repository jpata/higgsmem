#include <iostream>
#include <map>
#include <string>

#include <TFile.h>
#include <TROOT.h>

// general nanoflow framework declarations
#include "nanoflow.h"

// Higgs MEM
#include "me_hmumu.h"
#include "meanalyzer.h"
#include "myanalyzers.h"

using namespace nanoflow;

int main(int argc, char* argv[]) {
  using json = nlohmann::json;

  cout << get_time() << " looper main() started" << endl;
  gROOT->SetBatch(true);

  if (argc != 3) {
    cerr << "Usage: ./looper input.json output.json" << endl;
    return 0;
  }

  cout << get_time() << " loading json file " << argv[1] << endl;

  // Load the configuration from the provided input json file
  Configuration conf(argv[1]);

  // Create the output file
  Output output(conf.output_filename);

  // Define the sequence of analyzers you want to run
  // These are defined in the myanalyzers.h/myanalyzers.cc files
  vector<Analyzer*> analyzers = {new MuonEventAnalyzer(output),
                                 new MEMEventAnalyzer(output, 13000.0, "data/param_card_MH_125_00.dat"),
                                 //      new JetEventAnalyzer(*output),
                                 //      new GenJetEventAnalyzer(*output),
                                 //      new GenRecoJetMatchAnalyzer(*output),
                                 //      new ElectronEventAnalyzer(*output),
                                 //      new SumPtAnalyzer(*output),
                                 //      new EventVarsAnalyzer(*output),
                                 new LeptonPairAnalyzer(output),
                                 //      new JetDeltaRAnalyzer(*output),
                                 new MEMTreeAnalyzer(output)
  };

  // Define the final output report
  json total_report;

  // Loop over all the input files
  for (const auto& input_file : conf.input_files) {
    cout << "Opening input file " << input_file << endl;
    TFile* tf = TFile::Open(input_file.c_str());
    if (tf == nullptr) {
      cerr << "Could not open file " << input_file << ", exiting" << endl;
      return 1;
    }
    TTreeReader reader("Events", tf);

    auto report =
        looper_main<MEMEvent, Configuration>(conf, reader, output, analyzers);
    total_report.push_back(report);
    tf->Close();
  }
  cout << "All input files processed, saving output" << endl;
  output.close();

  cout << get_time() << " looper main() done on json file " << argv[1] << endl;

  // Write the output metadata json
  std::ofstream out_json(argv[2]);
  out_json << total_report.dump(4);

  return 0;
}
