#include "me_hmumu.h"

using namespace std;

MatrixElementHiggsMuMu::MatrixElementHiggsMuMu(string mg_card_path) : calibration_file(new TFile("data/mem_calibration.root"))
{
  cout << "Creating MatrixElementHiggsMuMu" << endl; 
  proc_qqz.initProc(mg_card_path.c_str());

  const vector<const char*> datacards = {
    "data/param_card_MH_100_00.dat",
    "data/param_card_MH_101_00.dat",
    "data/param_card_MH_102_00.dat",
    "data/param_card_MH_103_00.dat",
    "data/param_card_MH_104_00.dat",
    "data/param_card_MH_105_00.dat",
    "data/param_card_MH_106_00.dat",
    "data/param_card_MH_107_00.dat",
    "data/param_card_MH_108_00.dat",
    "data/param_card_MH_109_00.dat",
    "data/param_card_MH_110_00.dat",
    "data/param_card_MH_111_00.dat",
    "data/param_card_MH_112_00.dat",
    "data/param_card_MH_113_00.dat",
    "data/param_card_MH_114_00.dat",
    "data/param_card_MH_115_00.dat",
    "data/param_card_MH_116_00.dat",
    "data/param_card_MH_117_00.dat",
    "data/param_card_MH_118_00.dat",
    "data/param_card_MH_119_00.dat",
    "data/param_card_MH_120_00.dat",
    "data/param_card_MH_120_50.dat",
    "data/param_card_MH_121_00.dat",
    "data/param_card_MH_121_50.dat",
    "data/param_card_MH_122_00.dat",
    "data/param_card_MH_122_50.dat",
    "data/param_card_MH_123_00.dat",
    "data/param_card_MH_123_10.dat",
    "data/param_card_MH_123_20.dat",
    "data/param_card_MH_123_30.dat",
    "data/param_card_MH_123_40.dat",
    "data/param_card_MH_123_50.dat",
    "data/param_card_MH_123_60.dat",
    "data/param_card_MH_123_70.dat",
    "data/param_card_MH_123_80.dat",
    "data/param_card_MH_123_90.dat",
    "data/param_card_MH_124_00.dat",
    "data/param_card_MH_124_10.dat",
    "data/param_card_MH_124_20.dat",
    "data/param_card_MH_124_30.dat",
    "data/param_card_MH_124_40.dat",
    "data/param_card_MH_124_50.dat",
    "data/param_card_MH_124_60.dat",
    "data/param_card_MH_124_70.dat",
    "data/param_card_MH_124_80.dat",
    "data/param_card_MH_124_90.dat",
    "data/param_card_MH_125_00.dat",
    "data/param_card_MH_125_10.dat",
    "data/param_card_MH_125_20.dat",
    "data/param_card_MH_125_30.dat",
    "data/param_card_MH_125_40.dat",
    "data/param_card_MH_125_50.dat",
    "data/param_card_MH_125_60.dat",
    "data/param_card_MH_125_70.dat",
    "data/param_card_MH_125_80.dat",
    "data/param_card_MH_125_90.dat",
    "data/param_card_MH_126_00.dat",
    "data/param_card_MH_126_10.dat",
    "data/param_card_MH_126_20.dat",
    "data/param_card_MH_126_30.dat",
    "data/param_card_MH_126_40.dat",
    "data/param_card_MH_126_50.dat",
    "data/param_card_MH_126_60.dat",
    "data/param_card_MH_126_70.dat",
    "data/param_card_MH_126_80.dat",
    "data/param_card_MH_126_90.dat",
    "data/param_card_MH_127_00.dat",
    "data/param_card_MH_127_50.dat",
    "data/param_card_MH_128_00.dat",
    "data/param_card_MH_128_50.dat",
    "data/param_card_MH_129_00.dat",
    "data/param_card_MH_129_50.dat",
    "data/param_card_MH_130_00.dat",
    "data/param_card_MH_131_00.dat",
    "data/param_card_MH_132_00.dat",
    "data/param_card_MH_133_00.dat",
    "data/param_card_MH_134_00.dat",
    "data/param_card_MH_135_00.dat",
    "data/param_card_MH_136_00.dat",
    "data/param_card_MH_137_00.dat",
    "data/param_card_MH_138_00.dat",
    "data/param_card_MH_139_00.dat",
    "data/param_card_MH_140_00.dat",
    "data/param_card_MH_141_00.dat",
    "data/param_card_MH_142_00.dat",
    "data/param_card_MH_143_00.dat",
    "data/param_card_MH_144_00.dat",
    "data/param_card_MH_145_00.dat",
    "data/param_card_MH_146_00.dat",
    "data/param_card_MH_147_00.dat",
    "data/param_card_MH_148_00.dat",
    "data/param_card_MH_149_00.dat",
    "data/param_card_MH_150_00.dat",
  };

  for (unsigned int i=0; i<num_masspoints; i++) {
    procs_ggh[i].initProc(datacards[i]); 
  }

  ggh_pdf_logpz = (TH1D*)(*calibration_file).Get("ggh_pdf_logpz");
  qqZ_pdf_logpz = (TH1D*)(*calibration_file).Get("qqz_pdf_logpz");
  assert(ggh_pdf_logpz != nullptr);
  assert(qqZ_pdf_logpz != nullptr);
}

array<double, MatrixElementHiggsMuMu::num_masspoints> MatrixElementHiggsMuMu::compute_me_final_mumu_hypo(TLorentzVector total_fs, TLorentzVector f1, TLorentzVector f2, TH1D* h_logpz, function<array<double, MatrixElementHiggsMuMu::num_masspoints>(MatrixElementHiggsMuMu::pspoint)> compute_amplitude) {
  const auto E = total_fs.E();
  const auto pz = total_fs.Pz();
  
  unsigned int iloop = 0;
  int isuccess = 0;
  double amp_tot = 0.0;
  array<double, MatrixElementHiggsMuMu::num_masspoints> ret;
  for (unsigned int i=0; i<MatrixElementHiggsMuMu::num_masspoints; i++) {
    ret[i] = 0.0;
  }

  while ( iloop<500 ) {
    const auto rnd = h_logpz->GetRandom();
    const auto pz_i1 = exp(rnd);
    const auto pz_i2 = pz - pz_i1;

    TLorentzVector i1_reco(0, 0, pz_i1, abs(pz_i1));
    TLorentzVector i2_reco(0, 0, pz_i2, abs(pz_i2));

    const auto phase_space_point = make_phase_space_4_lv(i1_reco, i2_reco, f1, f2);
    ret = compute_amplitude(phase_space_point);
    
    if (ret[0] > 0.0) {
      break;
    }
    iloop += 1;
  }
  return ret;
}

MEValues MatrixElementHiggsMuMu::compute_me_final_mumu(TLorentzVector f1, TLorentzVector f2) {
  auto total_fs = f1 + f2;
  auto boost_beta = -TVector3(total_fs.Px()/total_fs.E(), total_fs.Py()/total_fs.E(), 0.0);
  f1.Boost(boost_beta);
  f2.Boost(boost_beta);
  total_fs = f1 + f2;

  MEValues ret;  

  ret.ggh_hmumu = compute_me_final_mumu_hypo(total_fs, f1, f2, ggh_pdf_logpz, [this](MatrixElementHiggsMuMu::pspoint ps){ return this->compute_amplitude_gghmumu(ps); });
  ret.qqz_zmumu = compute_me_final_mumu_hypo(total_fs, f1, f2, qqZ_pdf_logpz, [this](MatrixElementHiggsMuMu::pspoint ps){ return this->compute_amplitude_qqZmumu(ps); });
  ret.reco_fs_pz = total_fs.Pz();
  return ret;
}

MEValues MatrixElementHiggsMuMu::compute_me_initial_final_mumu(TLorentzVector i1, TLorentzVector i2, TLorentzVector f1, TLorentzVector f2) {
  const auto phase_space_point = make_phase_space_4_lv(i1, i2, f1, f2);
  MEValues ret;
  ret.ggh_hmumu = compute_amplitude_gghmumu(phase_space_point);
  ret.qqz_zmumu = compute_amplitude_qqZmumu(phase_space_point);
  return ret;
}


MatrixElementHiggsMuMu::pspoint MatrixElementHiggsMuMu::make_phase_space_4(
    MatrixElementHiggsMuMu::p4 i1, MatrixElementHiggsMuMu::p4 i2, MatrixElementHiggsMuMu::p4 f1, MatrixElementHiggsMuMu::p4 f2) {
  array<double, MatrixElementHiggsMuMu::num_external*MatrixElementHiggsMuMu::num_p4> phase_space_point;

  // Set initial state momentum (E,Px,Py,Pz,M)
  phase_space_point[0 * MatrixElementHiggsMuMu::num_p4 + 0] = i1[0];
  phase_space_point[0 * MatrixElementHiggsMuMu::num_p4 + 1] = i1[1];
  phase_space_point[0 * MatrixElementHiggsMuMu::num_p4 + 2] = i1[2];
  phase_space_point[0 * MatrixElementHiggsMuMu::num_p4 + 3] = i1[3];

  phase_space_point[1 * MatrixElementHiggsMuMu::num_p4 + 0] = i2[0];
  phase_space_point[1 * MatrixElementHiggsMuMu::num_p4 + 1] = i2[1];
  phase_space_point[1 * MatrixElementHiggsMuMu::num_p4 + 2] = i2[2];
  phase_space_point[1 * MatrixElementHiggsMuMu::num_p4 + 3] = i2[3];

  // Set final state momentum (E,Px,Py,Pz,M)
  phase_space_point[2 * MatrixElementHiggsMuMu::num_p4 + 0] = f1[0];
  phase_space_point[2 * MatrixElementHiggsMuMu::num_p4 + 1] = f1[1];
  phase_space_point[2 * MatrixElementHiggsMuMu::num_p4 + 2] = f1[2];
  phase_space_point[2 * MatrixElementHiggsMuMu::num_p4 + 3] = f1[3];

  phase_space_point[3 * MatrixElementHiggsMuMu::num_p4 + 0] = f2[0];
  phase_space_point[3 * MatrixElementHiggsMuMu::num_p4 + 1] = f2[1];
  phase_space_point[3 * MatrixElementHiggsMuMu::num_p4 + 2] = f2[2];
  phase_space_point[3 * MatrixElementHiggsMuMu::num_p4 + 3] = f2[3];

  return phase_space_point;
}

MatrixElementHiggsMuMu::pspoint MatrixElementHiggsMuMu::make_phase_space_4_lv(
    const TLorentzVector& i1, const TLorentzVector& i2, const TLorentzVector& f1, const TLorentzVector& f2) {
  return MatrixElementHiggsMuMu::make_phase_space_4({i1.Energy(), i1.Px(), i1.Py(), i1.Pz()},
                              {i2.Energy(), i2.Px(), i2.Py(), i2.Pz()},
                              {f1.Energy(), f1.Px(), f1.Py(), f1.Pz()},
                              {f2.Energy(), f2.Px(), f2.Py(), f2.Pz()});
}

array<double, MatrixElementHiggsMuMu::num_masspoints> MatrixElementHiggsMuMu::compute_amplitude_gghmumu(
    MatrixElementHiggsMuMu::pspoint phase_space_point) {
  
  vector<double*> p;
  p.push_back(&(phase_space_point[0]));
  p.push_back(&(phase_space_point[4]));
  p.push_back(&(phase_space_point[8]));
  p.push_back(&(phase_space_point[12]));

  array<double, num_masspoints> me_ret;
  for (unsigned int i=0; i < num_masspoints; i++) {
    auto& proc = procs_ggh[i];
    proc.setMomenta(p);
    proc.sigmaKin();
    const double* me = proc.getMatrixElements();
    me_ret[i] = me[0];
  }
  return me_ret;
}

array<double, MatrixElementHiggsMuMu::num_masspoints> MatrixElementHiggsMuMu::compute_amplitude_qqZmumu(
    MatrixElementHiggsMuMu::pspoint phase_space_point) {

  array<double, num_masspoints> me_ret;
  for (unsigned int i=0; i < num_masspoints; i++) {
    me_ret[i] = 0.0;
  } 
  vector<double*> p;
  p.push_back(&(phase_space_point[0]));
  p.push_back(&(phase_space_point[4]));
  p.push_back(&(phase_space_point[8]));
  p.push_back(&(phase_space_point[12]));

  proc_qqz.setMomenta(p);
  proc_qqz.sigmaKin();
  const double* me = proc_qqz.getMatrixElements();
  me_ret[0] = me[0];
  return me_ret;
}
