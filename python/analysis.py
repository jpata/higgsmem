#!/usr/bin/env python
import nanoflow
import ROOT
import logging

def setup():
    ROOT.gROOT.ProcessLine('.include interface')
    ROOT.gROOT.ProcessLine('.include src/madgraph')
   
    nanoflow.load_header("nanoflow.h") 
    nanoflow.load_header("meanalyzer.h") 
    nanoflow.load_header("myanalyzers.h") 
    
    nanoflow.load_lib("bin/libamp_hmm.so")    
    nanoflow.load_lib("bin/libhiggsmem.so")    

def run_looper(input_json, output_json):
    an = nanoflow.SequentialAnalysis(input_json, getattr(ROOT, "looper_main_higgsmem")) 

    an.add(ROOT.MuonEventAnalyzer(an.output))
    an.add(ROOT.MEMEventAnalyzer(an.output, 13000, "data/param_card.dat"))
    an.add(ROOT.GenLeptonEventAnalyzer(an.output))
    an.add(ROOT.LeptonPairAnalyzer(an.output))
    an.add(ROOT.MEMTreeAnalyzer(an.output))
    
    reports = an.run()
    an.save(reports, output_json)

def run_looper_args(args):
    run_looper(*args)

if __name__ == "__main__":
    
    logging.basicConfig(level=logging.INFO)
    analysis = nanoflow.Analysis.from_yaml("data/analysis.yaml")
    analysis.run_looper_args = run_looper_args
    datasets = analysis.get_datasets()
    analysis.copy_files(datasets)
    analysis.create_jobfiles(datasets, 1)
    #analysis.run_jobs(datasets, 1, setup)
