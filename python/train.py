import os, shutil
import numpy as np
import time
import argparse
import sys

os.environ["KERAS_BACKEND"] = "tensorflow"
import keras
from keras.callbacks import TensorBoard
import keras.backend as K
import tensorflow as tf

log_r_clip_value = 10.0

def load_data(path):
    data = np.load(path)
    
    X_sig = data["X_sig"]
    X_bkg = data["X_bkg"]
    
    y_sig = data["y_sig"][:, 46:47]
    y_bkg = data["y_bkg"][:, 46:47]
    
    yreco_sig = data["yreco_sig"][:, 46:47]
    yreco_bkg = data["yreco_bkg"][:, 46:47]
    
    y_sig = np.hstack([y_sig, np.ones_like(y_sig)])
    y_bkg = np.hstack([y_bkg, np.zeros_like(y_bkg)])
    
    z_sig = data["z_sig"]
    z_bkg = data["z_bkg"]
    
    return (X_sig, y_sig, yreco_sig, z_sig, X_bkg, y_bkg, yreco_bkg, z_bkg)

def split_test_train(X_sig, y_sig, yreco_sig, z_sig, X_bkg, y_bkg, yreco_bkg, z_bkg, mass_window=None):

    if mass_window:
        sel_sig = np.abs(z_sig[:, 0] - 125) < mass_window
        sel_bkg = np.abs(z_bkg[:, 0] - 125) < mass_window
        X_sig = X_sig[sel_sig] 
        X_bkg = X_bkg[sel_bkg] 
        y_sig = y_sig[sel_sig] 
        y_bkg = y_bkg[sel_bkg] 
        yreco_sig = yreco_sig[sel_sig] 
        yreco_bkg = yreco_bkg[sel_bkg] 
        z_sig = z_sig[sel_sig] 
        z_bkg = z_bkg[sel_bkg] 
    
    w_sig = (1e9/float(len(X_sig)))*np.ones(len(X_sig))
    w_bkg = (1e9/float(len(X_bkg)))*np.ones(len(X_bkg))
    
    X = np.vstack([X_sig, X_bkg])
    y = np.vstack([y_sig, y_bkg])
    yreco = np.vstack([yreco_sig, yreco_bkg])
    z = np.vstack([z_sig, z_bkg])
    w = np.hstack([w_sig, w_bkg])
    

    Xmeans = X.mean(axis=0)
    X = X - Xmeans
    Xstds = X.std(axis=0)
    X = X / Xstds
    
    ymeans = y[:, 0].mean(axis=0)
    y[:, 0] = y[:, 0] - ymeans
    ystds = y[:, 0].std(axis=0)
    y[:, 0] = y[:, 0] / ystds
    
    zmeans = z[:, 0].mean(axis=0)
    z[:, 0] = z[:, 0] - zmeans
    zstds = z[:, 0].std(axis=0)
    z[:, 0] = z[:, 0]/zstds
    
    ntrain = int(0.8*len(X))
    ntest = int(0.1*len(X))

    perm = np.random.permutation(len(X))
    
    X = X[perm]
    y = y[perm]
    yreco = yreco[perm]
    z = z[perm]
    w = w[perm]
 
    X_train = X[:ntrain]
    X_test = X[ntrain:ntrain+ntest]
    
    y_train = y[:ntrain]
    y_test = y[ntrain:ntrain+ntest]
    
    yreco_train = yreco[:ntrain]
    yreco_test = yreco[ntrain:ntrain+ntest]
    
    z_train = z[:ntrain]
    z_test = z[ntrain:ntrain+ntest]

    w_train = w[:ntrain]
    w_test = w[ntrain:ntrain+ntest]
    
    return X_train, y_train, yreco_train, z_train, w_train, X_test, y_test, yreco_test, z_test, w_test

def get_activation(act):
    return getattr(keras.activations, act)

def build_regression(inp, nlayers, nunits, activation):
    l = keras.layers.Dense(nunits, activation=activation)(inp)
    for i in range(nlayers):
        #l = keras.layers.BatchNormalization()(l)
        l = keras.layers.Dense(nunits, activation=activation)(l)
    out = keras.layers.Dense(1)(l)
    model = keras.models.Model(inp, out, name="regression")
    return model

def build_classifier(inp, nlayers, nunits, activation):
    l = keras.layers.Dense(nunits, activation=activation)(inp)
    for i in range(nlayers):
        #l = keras.layers.BatchNormalization()(l)
        l = keras.layers.Dense(nunits, activation=activation)(l)
    out = keras.layers.Dense(1, activation="sigmoid")(l)
    model = keras.models.Model(inp, out, name="classifier")
    return model

def build_adversarial(nlayers, nunits, activation):
    inp = keras.layers.Input(shape=(1,), name="r_input")
    l = keras.layers.Dense(nunits, activation=activation)(inp)
    for i in range(nlayers):
        #l = keras.layers.BatchNormalization()(l)
        l = keras.layers.Dense(nunits, activation=activation)(l)
    out = keras.layers.Dense(1)(l)
    model = keras.models.Model(inp, out, name="adversarial")
    return model


def build_classification_adversarial(inp, nlayers, nunits, lr, alpha, activation):
    """
    Builds a Keras model for binary classification with adversarial training for bias reduction.
    In a simple classifier, we fit
    X -> classifier -> y_pred, where Loss = sum_i bce(y_i, y_pred_i)
    where y_i = {0,1} for signal or background.
    
    In the adversarial case, we instead have a model
    X -> classifier -> y_pred -> adversarial regression -> z_pred
    where z_pred is a predicted "protected variable" that we want to decorrelate from the signal or background
    prediction y_pred. The loss function for classifier training is Loss = sum_i bce(y_i, y_pred_i) - alpha mse(z_i, z_pred_i),
    where alpha is a chosen weight (e.g. 1.0) to balance the classification and regression tasks.
    
    inp (Keras InputLayer) - the input layer (e.g. feature vector)
    nlayers (int) - the number of intermediate dense layers for both the classifier and regression
    nunits (int) - the number of dense layer units for both the classifeir and regression
    activation (string) - the keras activation function

    returns:
        classifier model, regression model,
        classifier*regression for classifier training,
        classifier*regression for regression training    
    """

    act = get_activation(activation)    
    
    #build the classifier model
    model_cls = build_classifier(inp, nlayers, nunits, activation)
    model_cls.summary()
    
    #build the regression model
    model_cls_adv = build_adversarial(nlayers, nunits, activation)
    model_cls_adv.summary()
    
    #Construct a combined model of input (inp) -> classifier (model_cls) -> classifier output (sc) -> regression (model_cls_adv) -> regression output (out)
    sc = model_cls(inp)
    out = model_cls_adv(sc)
   
    #Compile the classifier
    optimizer = keras.optimizers.Nadam(lr=lr)
    model_cls.compile(loss=keras.losses.binary_crossentropy, optimizer=optimizer)
 
    #Set the regression model (adversarial part) weights to be fixed
    model_cls_adv.trainable = False
    for l in model_cls_adv.layers:
        l.trainable = False
    
    #Construct the combined model (classifier -> regression), where only the classifier is trainable 
    model_cls_train = keras.models.Model(inp, [sc, out])
    model_cls_train.compile(loss=[keras.losses.binary_crossentropy, keras.losses.mse], optimizer=optimizer, loss_weights=[1.0, -alpha])
    model_cls_train.summary()
    
    #Set the regression part to be trainable, classifier to be fixed
    model_cls.trainable = False
    for l in model_cls.layers:
        l.trainable = False
    model_cls_adv.trainable = True
    for l in model_cls_adv.layers:
        l.trainable = True
    
    #Construct the combined model (classifier -> regression), where only the regression part (adversarial) is trainable 
    model_cls_adv_train = keras.models.Model(inp, out)
    model_cls_adv_train.compile(loss=keras.losses.mse, optimizer=optimizer)
    model_cls_adv_train.summary()
 
    return model_cls, model_cls_adv, model_cls_train, model_cls_adv_train

def ratio_regression(y_true, y_pred):
    s = y_true[:, 1]
    #s = tf.Print(s, [s], "s=", summarize=10)
    #y_pred = tf.Print(y_pred, [y_pred], "y_pred=", summarize=10)

    A = K.exp(K.clip(y_true[:, 0],  -log_r_clip_value, log_r_clip_value))
    #A = tf.Print(A, [A], "A=", summarize=10)
    B = K.exp(K.clip(y_pred[:, 0], -log_r_clip_value, log_r_clip_value))
    #B = tf.Print(B, [B], "B=", summarize=10)
    tf.summary.histogram("A", A)
    tf.summary.histogram("B", B)

    invA = K.exp(-K.clip(y_true[:, 0],  -log_r_clip_value, log_r_clip_value))
    #invA = tf.Print(invA, [invA], "invA=", summarize=10)
    invB = K.exp(-K.clip(y_pred[:, 0], -log_r_clip_value, log_r_clip_value))
    #invB = tf.Print(invB, [invB], "invB=", summarize=10)
    tf.summary.histogram("invA", invA)
    tf.summary.histogram("invB", invB)

    sqdiff1 = s*tf.squared_difference(A, B)
    sqdiff2 = (1.0 - s)*tf.squared_difference(invA, invB)
    #sqdiff1 = tf.Print(sqdiff1, [sqdiff1], "sqdiff1=", summarize=10)
    #sqdiff2 = tf.Print(sqdiff2, [sqdiff2], "sqdiff2=", summarize=10)
    tf.summary.histogram("sqdiff1", sqdiff1)
    tf.summary.histogram("sqdiff2", sqdiff2)

#    r_loss = K.square(A - B)
#    r_loss = tf.Print(r_loss, [r_loss], "r_loss=", summarize=10)
#
#    inv_r_loss = K.square(invA - invB)
#    inv_r_loss = tf.Print(inv_r_loss, [inv_r_loss], "inv_r_loss=", summarize=10)
#
#    total_loss = K.mean(s*r_loss + (1.0 - s)* inv_r_loss, axis=-1)
    total_loss = tf.reduce_mean(sqdiff1 + sqdiff2, axis=-1)
    return total_loss

def build_regression_adversarial(inp, nlayers, nunits, lr, alpha, activation):
    model_reg = build_regression(inp, nlayers, nunits, activation)
    model_reg.summary()
    
    model_adv = build_adversarial(nlayers, nunits, activation)
    model_adv.summary()
    
    sc = model_reg(inp)
    out = model_adv(sc)
    
    #Regression
    optimizer = keras.optimizers.Nadam(lr=lr)
    model_reg.compile(loss=ratio_regression, optimizer=optimizer)
    model_adv.trainable = False
    for l in model_adv.layers:
        l.trainable = False
    
    model_reg_train = keras.models.Model(inp, [sc, out])
    model_reg_train.compile(loss=[ratio_regression, keras.losses.mse], optimizer=optimizer, loss_weights=[1.0, -alpha])
    model_reg_train.summary()
    
    #Adversarial
    model_reg.trainable = False
    for l in model_reg.layers:
        l.trainable = False
    model_adv.trainable = True
    for l in model_adv.layers:
        l.trainable = True
        
    model_reg_adv_train = keras.models.Model(inp, out)
    model_reg_adv_train.compile(loss=keras.losses.mse, optimizer=optimizer)
    model_reg_adv_train.summary()
    
    return model_reg, model_adv, model_reg_train, model_reg_adv_train

def write_log(callback, names, logs, batch_no):
    for name, value in zip(names, logs):
        summary = tf.Summary()
        summary_value = summary.value.add()
        summary_value.simple_value = value
        summary_value.tag = name
        callback.writer.add_summary(summary, batch_no)
        callback.writer.flush()
    
def train_classification_adversarial(model_cls, model_cls_train, model_cls_adv_train, batch_size, nb_epochs, nmain, nadv):
    print "training classifier", len(X_train), len(X_test)
    losses_train_cls = []
    losses_test_cls = []

    tensorboard.set_model(model_cls_train)
     
    #y = [log(p_sig/p_bkg), p_sig/(p_sig+p_bkg), is_signal] 

    for epoch in range(nb_epochs):
        
        l0 = model_cls_train.evaluate(X_train, [y_train[:, 1], z_train[:, 0]], batch_size=batch_size, verbose=False, sample_weight=[w_train, w_train])
        l1 = model_cls_train.evaluate(X_test, [y_test[:, 1], z_test[:, 0]], batch_size=batch_size, verbose=False, sample_weight=[w_test, w_test])
        l2 = model_cls_adv_train.evaluate(X_train, z_train[:, 0], batch_size=batch_size, verbose=False, sample_weight=w_train)

        train_names = ['train_loss', 'train_cls_loss', 'train_adv_loss']
        val_names = ['val_loss', 'val_cls_loss', 'val_adv_loss']
        write_log(tensorboard, train_names, l0, epoch)
        write_log(tensorboard, val_names, l1, epoch)
 
        losses_train_cls += [l0]
        losses_test_cls += [l1]
        print epoch, l0, l1, l2

        for k in range(nadv):
            for ibatch in range(0, len(X_train), batch_size):
                xb = X_train[ibatch:ibatch+batch_size]
                yb = y_train[ibatch:ibatch+batch_size]
                zb = z_train[ibatch:ibatch+batch_size]
                wb = w_train[ibatch:ibatch+batch_size]
                l = model_cls_adv_train.train_on_batch(xb, zb[:, 0], sample_weight=wb)
                print epoch, k, ibatch, l
                sys.stdout.write("\033[F")
                sys.stdout.flush()
 
        for k in range(nmain):
            for ibatch in range(0, len(X_train), batch_size):
                xb = X_train[ibatch:ibatch+batch_size]
                yb = y_train[ibatch:ibatch+batch_size]
                zb = z_train[ibatch:ibatch+batch_size]
                wb = w_train[ibatch:ibatch+batch_size]
                l = model_cls_train.train_on_batch(xb, [yb[:, 1], zb[:, 0]], sample_weight=[wb, wb])
                print epoch, k, ibatch, "  ".join(["{0:.2E}".format(x) for x in l])
                sys.stdout.write("\033[F")
                sys.stdout.flush()
 
    return losses_train_cls, losses_test_cls

def train_regression_adversarial(model_reg, model_reg_train, model_reg_adv_train, batch_size, nb_epochs, nmain, nadv):
    print "training regression", len(X_train), len(X_test)
    losses_train_cls = []
    losses_test_cls = []

    tensorboard.set_model(model_reg_train)
    
    for epoch in range(nb_epochs):
        
        l0 = model_reg_train.evaluate(X_train, [y_train, z_train[:, 0]], batch_size=batch_size, verbose=False, sample_weight=[w_train, w_train])
        l1 = model_reg_train.evaluate(X_test, [y_test, z_test[:, 0]], batch_size=batch_size, verbose=False, sample_weight=[w_test, w_test])
        l2 = model_reg_adv_train.evaluate(X_train, z_train[:, 0], batch_size=batch_size, verbose=False, sample_weight=w_train)

        train_names = ['train_loss', 'train_reg_loss', 'train_adv_loss']
        val_names = ['val_loss', 'val_reg_loss', 'val_adv_loss']
        write_log(tensorboard, train_names, l0, epoch)
        write_log(tensorboard, val_names, l1, epoch)
 
        losses_train_cls += [l0]
        losses_test_cls += [l1]
        print epoch, l0, l1, l2

        for k in range(nadv):
            for ibatch in range(0, len(X_train), batch_size):
                xb = X_train[ibatch:ibatch+batch_size]
                yb = y_train[ibatch:ibatch+batch_size]
                zb = z_train[ibatch:ibatch+batch_size]
                wb = w_train[ibatch:ibatch+batch_size]
                l = model_reg_adv_train.train_on_batch(xb, zb[:, 0], sample_weight=wb)
                print epoch, k, ibatch, l
                sys.stdout.write("\033[F")
                sys.stdout.flush()

        for k in range(nmain):
            for ibatch in range(0, len(X_train), batch_size):
                xb = X_train[ibatch:ibatch+batch_size]
                yb = y_train[ibatch:ibatch+batch_size]
                zb = z_train[ibatch:ibatch+batch_size]
                wb = w_train[ibatch:ibatch+batch_size]
                l = model_reg_train.train_on_batch(xb, [yb, zb[:, 0]], sample_weight=[wb, wb])
                print epoch, k, ibatch, "  ".join(["{0:.2E}".format(x) for x in l])
                sys.stdout.write("\033[F")
                sys.stdout.flush()
 
    return losses_train_cls, losses_test_cls

def save_model(model, outdir):
    model_json = model.to_json()
    os.makedirs(outdir)
    with open(outdir + "/model.json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights(outdir + "/model.h5")

if __name__ == "__main__":
    (X_sig, y_sig, z_sig, X_bkg, y_bkg, z_bkg) = load_data()
    X_train, y_train, z_train, w_train, X_test, y_test, z_test, w_test = split_test_train(X_sig, y_sig, z_sig, X_bkg, y_bkg, z_bkg)
 
    inp = keras.layers.Input(shape=(X_train.shape[1], ), name='momentum_input')
    parser = argparse.ArgumentParser()
    parser.add_argument("--nlayers", type=int, required=True)
    parser.add_argument("--nunits", type=int, required=True)
    parser.add_argument("--lr", type=float, required=True)
    parser.add_argument("--alpha", type=float, required=True)
    parser.add_argument("--activation", type=str, required=True, choices=["relu", "selu", "elu", "tanh"])
    parser.add_argument("--model", type=str, required=True, choices=["cls", "reg"])
    parser.add_argument("--nmain", type=int, required=True)
    parser.add_argument("--nadv", type=int, required=True)
    args = parser.parse_args()

    args_id = "_".join(["{0}={1}".format(a, b) for (a, b) in sorted(vars(args).items(), key=lambda x: x[0])])
    uniq_name = "hmumu_{0}_{1}".format(time.time(), args_id)
    tensorboard = TensorBoard("/tmp/jpata/tensorboard/{0}".format(uniq_name))

    batch_size = 10000
    nb_epochs = 1000
    
    if args.model == "cls":
        model_cls, model_cls_adv, model_cls_train, model_cls_adv_train = build_classification_adversarial(inp, args.nlayers, args.nunits, args.lr, args.alpha, args.activation)
        losses_train_cls, losses_test_cls = train_classification_adversarial(model_cls, model_cls_train, model_cls_adv_train, batch_size, nb_epochs, args.nmain, args.nadv) 
   	model = model_cls
    elif args.model == "reg":
        model_reg, model_reg_adv, model_reg_train, model_reg_adv_train = build_regression_adversarial(inp, args.nlayers, args.nunits, args.lr, args.alpha, args.activation)
        losses_train_cls, losses_test_cls = train_regression_adversarial(model_reg, model_reg_train, model_reg_adv_train, batch_size, nb_epochs, args.nmain, args.nadv) 
   	model = model_reg
 
    outdir = "/tmp/jpata/keras_outputs/{0}".format(uniq_name)
    if os.path.isdir(outdir):
        shutil.rmtree(outdir)
    save_model(model, outdir) 
